package employee;

public class EmployeeCalculateAppraisal {

    public double calculateAppraisal(EmployeeDetails employeeDetails) {
        double appraisal = 0;

        if(employeeDetails.getMonthlySalary() < 10000){
            appraisal = 500;
        }else{
            appraisal = 1000;
        }

        return appraisal;
    }
}
