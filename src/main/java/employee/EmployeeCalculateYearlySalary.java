package employee;

public class EmployeeCalculateYearlySalary {

    public double calculateYearlySalary(EmployeeDetails employeeDetails){
        double yearlySalary = 0;
        yearlySalary = employeeDetails.getMonthlySalary() * 12;
        return yearlySalary;
    }
}
