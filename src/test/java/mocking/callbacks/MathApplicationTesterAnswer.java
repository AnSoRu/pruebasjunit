package mocking.callbacks;

import mocking.CalculatorService;
import mocking.MathApplication;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MathApplicationTesterAnswer {

    private MathApplication mathApplication;
    private CalculatorService calcService;

    @Before
    public void setUp(){
        mathApplication = new MathApplication();
        calcService = mock(CalculatorService.class);
        mathApplication.setCalculatorService(calcService);
    }

    @Test
    public void testAdd(){

        //add the behavior to add numbers
        when(calcService.add(20.0,10.0)).thenAnswer((Answer<Double>) invocation -> {
            //get the arguments passed to mock
            Object[] args = invocation.getArguments();

            //get the mock
            Object mock = invocation.getMock();

            //return the result
            return 30.0;
        });

        //test the add functionality
        Assert.assertEquals(mathApplication.add(20.0, 10.0),30.0,0);
    }
}
