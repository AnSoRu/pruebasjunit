package tests2;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestJunit2 extends TestCase {

    protected double fValue1;
    protected double fValue2;

    @Before
    protected void setUp() throws Exception {
        fValue1 = 2.0;
        fValue2 = 2.0;
    }

    @Test
    public void testAdd(){
        System.out.println("No of Test Case = " + this.countTestCases());

        String name = this.getName();
        System.out.println("Test Case Name = " + name);

        this.setName("testNewAdd");
        String newName = this.getName();
        System.out.println("Updated Test Case Name = " + newName);
        assertEquals(fValue1,fValue2);

    }

    @After
    protected void tearDown() throws Exception {
        fValue1 = 0.0;
        fValue2 = 0.0;
    }
}
