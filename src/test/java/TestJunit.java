import org.junit.Test;
import testsuite.MessageUtil;

import static org.junit.Assert.assertEquals;

public class TestJunit {

    private String message = "Hello World!";
    MessageUtil messageUtil = new MessageUtil(message);

    @Test
    public void testAdd(){
        String str = "Junit is working fine";
        assertEquals("Junit is working fine",str);
    }

    @Test
    public void testPrintMessage(){
        //message="New World!";
        assertEquals(message,messageUtil.printMessage());
    }
}
