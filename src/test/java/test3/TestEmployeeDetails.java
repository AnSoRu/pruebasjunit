package test3;


import employee.EmployeeCalculateAppraisal;
import employee.EmployeeCalculateYearlySalary;
import employee.EmployeeDetails;
import junit.framework.TestCase;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestEmployeeDetails extends TestCase {

    private EmployeeDetails employeeDetails;
    private EmployeeCalculateYearlySalary employeeCalculateYearlySalary;
    private EmployeeCalculateAppraisal employeeCalculateAppraisal;

    //Diferencia entre @Before y @BeforeClass
    //@Before -> se ejecuta antes de cada test
    //@BeforeClass -> se ejecuta UNA SOLA VEZ antes de cualquiera de los test de la clase

    @BeforeClass
    protected void setUp() throws Exception {
        this.employeeDetails = new EmployeeDetails();
        this.employeeCalculateYearlySalary = new EmployeeCalculateYearlySalary();
        this.employeeCalculateAppraisal = new EmployeeCalculateAppraisal();
    }

    @Test
    public void testCalculateAppraisalWhenMonthlySalaryLowerThan10000(){
        employeeDetails.setName("Rajeev");
        employeeDetails.setAge(25);
        employeeDetails.setMonthlySalary(8000);

        double appraisal = employeeCalculateAppraisal.calculateAppraisal(employeeDetails);
        assertEquals(500, appraisal, 0.0);
    }

    @Test
    public void testCalculateAppraisalWhenMonthlySalaryHigherThan10000(){
        employeeDetails.setName("Rajeev");
        employeeDetails.setAge(25);
        employeeDetails.setMonthlySalary(10001);

        double appraisal = employeeCalculateAppraisal.calculateAppraisal(employeeDetails);
        assertEquals(1000, appraisal, 0.0);
    }

    @Test
    public void testCalculateYearlySalary(){
        employeeDetails.setName("Rajeev");
        employeeDetails.setAge(25);
        employeeDetails.setMonthlySalary(8000);

        double salary = employeeCalculateYearlySalary.calculateYearlySalary(employeeDetails);
        assertEquals(96000, salary, 0.0);
    }

    @Test
    public void testEmployeeDetails(){
        employeeDetails.setName("Rajeev");
        employeeDetails.setAge(25);
        employeeDetails.setMonthlySalary(8000);

        assertEquals("Rajeev",employeeDetails.getName());
        assertEquals(25,employeeDetails.getAge());
    }
}
