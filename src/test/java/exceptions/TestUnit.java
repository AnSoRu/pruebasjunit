package exceptions;

import junit.framework.TestCase;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;


public class TestUnit { //Se ha eliminado el extends TestCase porque no funcionaba la comprobación del expected Exception

    private static String message;
    private static MessageUtil messageUtil;

    @BeforeClass
    public static void setUp() throws Exception {
        message = "Robert";
        messageUtil = new MessageUtil(message);
    }

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test (expected = ArithmeticException.class) //Si no funciona el expected puede deberse a que la clase extiende de TestCase. Eliminar esta extensión
    public void testPrintMessageCheckArithmeticExceptionIsThrown() {
        System.out.println("Inside testPrintMessage()");
        messageUtil.printMessage();
    }

    @Test
    public void testSalutationMessage() {
        System.out.println("Inside testSalutationMessage()");
        message = "Hi!" + "Robert";
        assertEquals(message,messageUtil.salutationMessage());
    }

    @AfterClass
    public static void tearDown() throws Exception {
        message = null;
        messageUtil = null;
    }
}
